package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Grafo {
	private ArrayList<Integer> vertices;
	ArrayList<HashSet<Integer>> LVecinos;
	private Map<Integer,Double>pesosDeVertices;
	
	public Grafo() {		
		this.vertices = new ArrayList <>();		 
		this.pesosDeVertices= new HashMap<>();
		this.LVecinos = new ArrayList<HashSet<Integer>>();
		for (int i = 0; i < vertices.size(); i++) {
			LVecinos.add(new HashSet<Integer>());
		}		
	}

	public Grafo(Grafo otroGrafo) {		
		this.vertices = otroGrafo.consultarVertices();
		this.pesosDeVertices= otroGrafo.obtenerPesosDeVertices();
		this.LVecinos = otroGrafo.obtenerVecinos();
		for (int i = 0; i < otroGrafo.consultarVertices().size(); i++) {
			LVecinos.add(new HashSet<Integer>());
		}
	}
	
	public void completar() {
		for (Integer origen : vertices)
			for (Integer destino : vertices)
				if (origen != destino && !existeArco(origen, destino))
					agregarArco(origen, destino);
	}

	public void agregarArco(Integer origen, Integer destino) {
		validarArco(origen, destino);
		LVecinos.get(obtenerIndiceDelVertice(origen)).add(destino);
		LVecinos.get(obtenerIndiceDelVertice(destino)).add(origen);
	}

	private void validarArco(Integer origen, Integer destino) {
		if (existeArco(origen, destino))
			throw new IllegalArgumentException("Esta arista ya existe");
		if (origen == destino)
			throw new IllegalArgumentException("No se permiten loops; (" + origen + ", " + destino + ")");
	}
	
	public void agregarVertice(Integer coordenada) {
	    if (coordenada == null) {
	        throw new IllegalArgumentException("La coordenada no puede ser nula");
	    }
        if (!(coordenada instanceof Integer)) {
            throw new IllegalArgumentException("El vértice debe ser un entero.");
        }
	    
	    LVecinos.add(new HashSet<>());
	    if (obtenerIndiceDelVertice(coordenada) == -1) {
	        vertices.add(coordenada);
	    }
	}

	public ArrayList<Integer> consultarVertices() {
		return this.vertices;
	}

	public ArrayList<HashSet<Integer>> consultarVecinos() {
		return this.LVecinos;
	}

	public int consultarTamanio() {
		return this.vertices.size();
	}

	public boolean existeArco(Integer origen, Integer destino) {
		return !LVecinos.isEmpty() && LVecinos.get(obtenerIndiceDelVertice(origen)).contains(destino);
	}

	public HashSet<Integer> obtenerVecinosDe(Integer vertice) {
		return LVecinos.get(obtenerIndiceDelVertice(vertice));
	}
	
	public ArrayList<HashSet<Integer>> obtenerVecinos() {
		return LVecinos;
	}
	
    public int obtenerIndiceDelVertice(int unVertice) {
        return vertices.indexOf(unVertice);
    }        
	
	public Map<Integer, Double> obtenerPesosDeVertices() {
		return pesosDeVertices;
	}

	public void agregarPesosDeVertices(Integer vertice, Double pesoVertice) {
		this.pesosDeVertices.put(vertice, pesoVertice);		
	}
	
	public Double obtenerPesoDeVertice(Integer vertice) {
		return this.pesosDeVertices.get(vertice);
	}

	public boolean existeVertice(int nodo) {
		return vertices.contains(nodo);		 
	}

}