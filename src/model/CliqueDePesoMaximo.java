package model;

public class CliqueDePesoMaximo {
	private Grafo grafo;

	public CliqueDePesoMaximo(Grafo grafo) {
		this.grafo = grafo;
	}

	public Grafo obtenerCliquePesoMax() { 
		while(esClique() == false && grafo.consultarTamanio() > 0){	
				quitarVerticeMenorPeso();							
		}
		return grafo;							
	}
	
	public void quitarVerticeMenorPeso() {
		double menorPeso = grafo.obtenerPesoDeVertice(grafo.consultarVertices().get(0));	
		int verticeConMenorPeso = grafo.consultarVertices().get(0);
		for(Integer vertice : grafo.consultarVertices()) {
			if(grafo.obtenerPesoDeVertice(vertice) < menorPeso) {  			
				menorPeso = grafo.obtenerPesoDeVertice(vertice);
				verticeConMenorPeso = vertice;
			}
		}
		grafo.obtenerVecinos().remove(grafo.obtenerIndiceDelVertice(verticeConMenorPeso));
		grafo.consultarVertices().remove(grafo.obtenerIndiceDelVertice(verticeConMenorPeso));
	}
	
	public boolean esClique() {										
		for (Integer origen: grafo.consultarVertices()) { 
			for (Integer destino: grafo.consultarVertices()) {
				if(origen != destino && grafo.existeArco(origen, destino)== false) {
					return false;
				}
			}
		}
		return true;
	}
	
}