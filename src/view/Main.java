package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import model.CliqueDePesoMaximo;
import model.Grafo;

public class Main {
	private JFrame frmCliquesGolosas;
	private Grafo grafo;
	

	private Map<Integer, int[]> coordenadasVertices;
	private JComboBox<String> comboBoxVerticeA;
	private JComboBox<String> comboBoxVerticeB;
	private DibujoGrafo panelDibujo;
	private JButton btnGenerarCliquePeso;
	private JButton btnAgregarArco;

	public Main() {
		
		grafo = new Grafo();
		coordenadasVertices = new HashMap<>();
		initialize();
	}

	private void initialize() {
		frmCliquesGolosas = new JFrame();
		frmCliquesGolosas.setResizable(false);
		frmCliquesGolosas.setTitle("Cliques Golosas");
		frmCliquesGolosas.setBounds(410, 160, 685, 460);
		frmCliquesGolosas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCliquesGolosas.getContentPane().setLayout(null);
		frmCliquesGolosas.setLocationRelativeTo(null);

		comboBoxVerticeA = new JComboBox<>();
		comboBoxVerticeA.setBounds(209, 30, 100, 22);
		frmCliquesGolosas.getContentPane().add(comboBoxVerticeA);

		comboBoxVerticeB = new JComboBox<>();
		comboBoxVerticeB.setBounds(345, 30, 100, 22);
		frmCliquesGolosas.getContentPane().add(comboBoxVerticeB);

		JButton btnNewButton = new RoundedButton("Crear Nodo");
		btnNewButton.setBounds(60, 30, 112, 22);
		frmCliquesGolosas.getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(e -> crearNodo());

		JLabel lblNodoA = new JLabel("Nodo A");
		lblNodoA.setForeground(new Color(255, 255, 255));
		lblNodoA.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNodoA.setBounds(209, 11, 89, 14);
		frmCliquesGolosas.getContentPane().add(lblNodoA);

		JLabel lblNodoB = new JLabel("Nodo B");
		lblNodoB.setForeground(new Color(255, 255, 255));
		lblNodoB.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNodoB.setBounds(345, 11, 89, 14);
		frmCliquesGolosas.getContentPane().add(lblNodoB);

		btnAgregarArco = new RoundedButton("Agregar Arco");
		btnAgregarArco.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAgregarArco.setBounds(465, 30, 112, 22);
		btnAgregarArco.setEnabled(false);
		frmCliquesGolosas.getContentPane().add(btnAgregarArco);
		btnAgregarArco.addActionListener(e -> dibujarArco());

		JButton btnLimpiar = new JButton();
		btnLimpiar.setIcon(new ImageIcon(Main.class.getResource("/view/wipe.png")));
		btnLimpiar.setBounds(598, 30, 48, 43);
		btnLimpiar.setToolTipText("Limpiar pantalla");
		frmCliquesGolosas.getContentPane().add(btnLimpiar);
		btnLimpiar.addActionListener(e -> limpiar());

		btnGenerarCliquePeso = new RoundedButton("Generar clique peso maximo");
		btnGenerarCliquePeso.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnGenerarCliquePeso.setBounds(241, 76, 189, 22);
		btnGenerarCliquePeso.setEnabled(false);
		frmCliquesGolosas.getContentPane().add(btnGenerarCliquePeso);

		panelDibujo = new DibujoGrafo(grafo, grafo.consultarVertices(), coordenadasVertices);
		panelDibujo.setBounds(60, 115, 550, 300);
		frmCliquesGolosas.getContentPane().add(panelDibujo);

        JLabel fondoMenu = new JLabel();   
        ImageIcon imagenFondo = new ImageIcon("src/view/fondo-main.jpg");
        Image imagenRedimensionada = imagenFondo.getImage().getScaledInstance(685, 460, Image.SCALE_SMOOTH);    
        fondoMenu.setIcon(new ImageIcon(imagenRedimensionada));    
        fondoMenu.setBounds(0, 0, 685, 460);
        frmCliquesGolosas.getContentPane().add(fondoMenu);
        
        generarCliquePesoMax();
    }

	private void crearNodo() {
	    try {
	        String nodoStr = JOptionPane.showInputDialog(frmCliquesGolosas, "Ingrese un nodo:");
	        if (nodoStr == null || nodoStr.trim().isEmpty()) {
	            JOptionPane.showMessageDialog(frmCliquesGolosas, "Debe ingresar un número entero.");
	            return;
	        }

	        int nodo = Integer.parseInt(nodoStr);

	        String pesoStr = JOptionPane.showInputDialog(frmCliquesGolosas, "Ingrese el peso:");
	        if (pesoStr == null || pesoStr.trim().isEmpty()) {
	            JOptionPane.showMessageDialog(frmCliquesGolosas, "Debe ingresar un número para el peso.");
	            return;
	        }

	        double peso = Double.parseDouble(pesoStr);

	        if (!grafo.existeVertice(nodo)) {
	            grafo.agregarVertice(nodo);
	            grafo.agregarPesosDeVertices(nodo, peso);
	            agregarVerticeAComboBox(nodoStr, comboBoxVerticeA);
	            agregarVerticeAComboBox(nodoStr, comboBoxVerticeB);
	            agregarCoordenadasVertice(nodo);
	            panelDibujo.repaint();
	            btnGenerarCliquePeso.setEnabled(true);
	            btnAgregarArco.setEnabled(true);
	        } else {
	            JOptionPane.showMessageDialog(frmCliquesGolosas, "El vértice ya existe.");
	        }
	    } catch (NumberFormatException ex) {
	        JOptionPane.showMessageDialog(frmCliquesGolosas, "El valor ingresado debe ser un número entero.");
	    }
	}

	private void agregarVerticeAComboBox(String nodo, JComboBox<String> comboBox) {
		if (!existeVerticeEnComboBox(nodo, comboBox)) {
			comboBox.addItem(nodo);
		}
	}

	private boolean existeVerticeEnComboBox(String nodo, JComboBox<String> comboBox) {
		for (int i = 0; i < comboBox.getItemCount(); i++) {
			if (comboBox.getItemAt(i).equals(nodo)) {
				return true;
			}
		}
		return false;
	}

	private void agregarCoordenadasVertice(Integer vertice) {
		int x = (int) (Math.random() * (panelDibujo.getWidth() - 40));
		int y = (int) (Math.random() * (panelDibujo.getHeight() - 40));
		coordenadasVertices.put(vertice, new int[] { x, y });
	}

	private void dibujarArco() {
		String nodo1Str = (String) comboBoxVerticeA.getSelectedItem();
		String nodo2Str= (String) comboBoxVerticeB.getSelectedItem();
		Integer nodo1= Integer.parseInt(nodo1Str);
		Integer nodo2= Integer.parseInt(nodo2Str);

		if (nodo1 == null || nodo2 == null || nodo1.equals(nodo2)) {
			JOptionPane.showMessageDialog(frmCliquesGolosas,
					"Seleccione nodos válidos y diferentes para origen y destino.");
			return;
		}

		grafo.agregarArco(nodo1, nodo2);
		panelDibujo.repaint();
	}

	private void generarCliquePesoMax() {
		btnGenerarCliquePeso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {			
				if (grafo.consultarVertices().isEmpty()) {
					JOptionPane.showMessageDialog(frmCliquesGolosas,
							"El grafo es vacío. No se puede generar una clique.");
					return;
				}
				
				CliqueDePesoMaximo cliquePM = new CliqueDePesoMaximo(new Grafo(grafo));
				agregarLineaYMarcador(cliquePM.obtenerCliquePesoMax());
			} 
		});
	}
	
	private void agregarLineaYMarcador(Grafo grafo) {
		 Map<Integer, int[]> coordenadas= new HashMap<>();
		for (Integer vertice : grafo.consultarVertices()) {
			coordenadas.put(vertice, coordenadasVertices.get(vertice));
		}
		 
		panelDibujo = new DibujoGrafo(grafo, grafo.consultarVertices(), coordenadas, Color.GREEN);
		panelDibujo.setBounds(60, 115, 550, 300);
		frmCliquesGolosas.getContentPane().add(panelDibujo);
		panelDibujo.repaint();
	}
	
	public Window getVentana() {
		return null;
	}
	
	private void limpiar() {
	    frmCliquesGolosas.dispose();
	    EventQueue.invokeLater(() -> {
	        try {
	            Main window = new Main();
	            window.setVisible(true);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    });
	}

	
	public void setVisible(Boolean visible) {
		frmCliquesGolosas.setVisible(visible);
	}
}