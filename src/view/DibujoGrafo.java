package view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JPanel;
import model.Grafo;

@SuppressWarnings("serial")

public class DibujoGrafo extends JPanel {
    private Grafo grafo;
    private ArrayList<Integer> vertices;
    private Map<Integer, int[]> coordenadasVertices;
    private Color color;

    public DibujoGrafo(Grafo grafo, ArrayList<Integer> vertices, Map<Integer, int[]> coordenadasVertices) {
        this(grafo, vertices, coordenadasVertices, null);
    }

    public DibujoGrafo(Grafo grafo, ArrayList<Integer> vertices, Map<Integer, int[]> coordenadasVertices, Color color) {
        this.grafo = grafo;
        this.vertices = vertices;
        this.coordenadasVertices = coordenadasVertices;
        this.color = color;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());

        if (grafo != null && coordenadasVertices != null) {
            drawEdges(g);
            drawVertices(g);
        }
    }

    private void drawEdges(Graphics g) {
        for (Integer vertice : vertices) {
            int[] coordsVertice = coordenadasVertices.get(vertice);
            if (coordsVertice != null) {
                int x1 = coordsVertice[0] + 10;
                int y1 = coordsVertice[1] + 10;

                for (Integer vecino : grafo.obtenerVecinosDe(vertice)) {
                    int[] coordsVecino = coordenadasVertices.get(vecino);
                    if (coordsVecino != null) {
                        int x2 = coordsVecino[0] + 10;
                        int y2 = coordsVecino[1] + 10;

                        g.setColor(color == null ? Color.RED : this.color);
                        g.drawLine(x1, y1, x2, y2);
                    }
                }
            }
        }
    }

    private void drawVertices(Graphics g) {
        for (Map.Entry<Integer, int[]> entry : coordenadasVertices.entrySet()) {
            Integer vertice = entry.getKey();
            int[] coords = entry.getValue();
            if (coords != null) {
                int x = coords[0];
                int y = coords[1];

                g.setColor(Color.BLUE);
                g.fillOval(x, y, 20, 20);

                g.setColor(Color.WHITE);
                String numeroVertice = vertice.toString();
                int widthNumero = g.getFontMetrics().stringWidth(numeroVertice);
                g.drawString(numeroVertice, x + (20 - widthNumero) / 2, y + 15);

                g.setColor(Color.BLACK);
                String pesoVertice = String.valueOf(grafo.obtenerPesoDeVertice(vertice));
                g.drawString(pesoVertice, x + 25, y + 15);
            }
        }
    }
}
