package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainVentana {

	private JFrame frmMenu;
	private JFrame frame;
	Main ventanaJuego;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainVentana window = new MainVentana();
					window.frmMenu.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainVentana() {
		initialize();
	}

	private void initialize() {
		crearMenuInicial();
		crearBotonComenzar();
	}
	
	private void crearMenuInicial() {
		crearVentana();
		crearBotonInstrucciones();
		crearBotonComenzar();
		insertarImagenDeFondo();
	}
	
	private void crearVentana() {
	    frmMenu = new JFrame();
	    frmMenu.setTitle("Cliques Golosas");
	    frmMenu.setBounds(0, 0, 678, 460);
	    frmMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frmMenu.getContentPane().setLayout(null);
	    frmMenu.setLocationRelativeTo(null);
		
        JLabel TituloMenu = new JLabel();
        ImageIcon tituloIcon = new ImageIcon("src/view/title.png");
        TituloMenu.setIcon(tituloIcon);
        TituloMenu.setBounds(180, 50, tituloIcon.getIconWidth(), tituloIcon.getIconHeight());
        
        frmMenu.getContentPane().add(TituloMenu);
        frmMenu.setLocationRelativeTo(null);
    }

	
	private void crearBotonInstrucciones() {
	    JButton BotonInstrucciones = new JButton("Instrucciones");
	    BotonInstrucciones.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	            frame = new JFrame();
	            frame.setResizable(false);
	            frame.setBounds(100, 100, 678, 460);
	            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	            frame.setTitle("Instrucciones");
	            

	            JPanel panelPrincipal = new JPanel(new BorderLayout());
	            panelPrincipal.setBackground(Color.BLACK);
	            
	            JLabel titulo = new JLabel("INSTRUCCIONES", SwingConstants.CENTER);
	            titulo.setForeground(Color.WHITE);
	            titulo.setFont(new Font("Lucida Console", Font.BOLD, 16));
	            
	            JTextArea Instrucciones = new JTextArea();
	            Instrucciones.setEditable(false);
	            Instrucciones.setForeground(Color.WHITE);
	            Instrucciones.setBackground(Color.BLACK);
	            Instrucciones.setFont(new Font("Lucida Console", Font.PLAIN, 13));
	            Instrucciones.setLineWrap(true);
	            Instrucciones.setWrapStyleWord(true);
	            Instrucciones.setText("\nEsta aplicación implementa un algoritmo goloso para resolver el problema de la clique de peso máximo.\n\n"
	                + "Dado un grafo, una clique es un conjunto de vértices tal que todos los vértices del conjunto son vecinos entre sí. "
	                + "El peso de una clique es igual a la suma de los pesos de sus vértices. El objetivo es encontrar una clique con el mayor peso posible.\n\n"
	                + "Para utilizar la aplicación, siga los siguientes pasos:\n\n"
	                + "1. Utilice el botón 'CREAR NODO' para ingresar los nodos deseados junto con sus respectivos pesos.\n\n"
	                + "2. Seleccione en 'NODO A' y 'NODO B' los nodos que desea unir y presione 'AGREGAR ARCO' para unirlos y formar el grafo.\n\n"
	                + "3. Una vez que haya ingresado todos los nodos y formado los arcos deseados, presione 'GENERAR CLIQUE DE PESO MÁXIMO' "
	                + "para obtener la clique de peso máximo.\n\n");
	            
	            panelPrincipal.add(titulo, BorderLayout.NORTH);
	            panelPrincipal.add(new JScrollPane(Instrucciones), BorderLayout.CENTER);
	            
	            JButton volverMenuPrincipal = new JButton("Volver al Menú Principal");
	            volverMenuPrincipal.setBackground(new Color(128, 128, 128));
	            volverMenuPrincipal.setFont(new Font("Lucida Console", Font.BOLD, 12));
	            volverMenuPrincipal.addActionListener(new ActionListener() {
	                public void actionPerformed(ActionEvent e) {
	                    frmMenu.setVisible(true);
	                    frame.setVisible(false);
	                }
	            });
	            volverMenuPrincipal.setVerticalAlignment(SwingConstants.TOP);
	            panelPrincipal.add(volverMenuPrincipal, BorderLayout.SOUTH);
	            
	            frame.getContentPane().add(panelPrincipal);
	            frame.setVisible(true);
	            frmMenu.setVisible(false);
	            frame.setLocationRelativeTo(null);
	        }
	    });
	    BotonInstrucciones.setSelected(true);
	    BotonInstrucciones.setForeground(new Color(51, 0, 51));
	    BotonInstrucciones.setFont(new Font("Tahoma", Font.PLAIN, 20));
	    BotonInstrucciones.setBounds(207, 313, 258, 42);
	    frmMenu.getContentPane().add(BotonInstrucciones);
	}

	private void crearBotonComenzar() {
		JButton BotonComenzar = new JButton("Iniciar");
		BotonComenzar.setForeground(new Color(51, 0, 51));
		BotonComenzar.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				ventanaJuego = new Main();
				ventanaJuego.setVisible(true);
				frmMenu.setVisible(false);
			}
		});
	
		BotonComenzar.setSelected(true);
		BotonComenzar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		BotonComenzar.setBounds(207, 238, 258, 42);
		frmMenu.getContentPane().add(BotonComenzar);	
	}
	
	private void insertarImagenDeFondo() {
	    JLabel fondoMenu = new JLabel();   
	    ImageIcon imagenFondo = new ImageIcon("src/view/fondo-menu.png");
	    Image imagenRedimensionada = imagenFondo.getImage().getScaledInstance(685, 460, Image.SCALE_SMOOTH);    
	    fondoMenu.setIcon(new ImageIcon(imagenRedimensionada));    
	    fondoMenu.setBounds(0, 0, 685, 460);
	    frmMenu.getContentPane().add(fondoMenu);
	}
}