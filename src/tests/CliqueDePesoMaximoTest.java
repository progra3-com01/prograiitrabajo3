package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import org.junit.Test;

import model.CliqueDePesoMaximo;
import model.Grafo;

public class CliqueDePesoMaximoTest {

	@Test
	public void obtenerCliquePesoMaxCasoFeliz1Test() {
		ArrayList<Integer> vertices = new ArrayList<>();
		Integer v1 = 2;
		Integer v2 = 3;
		Integer v3 = 5;
		Integer v4 = 6;
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		Grafo grafo = new Grafo();
		
		grafo.agregarVertice(v1);
		grafo.agregarVertice(v2);
		grafo.agregarVertice(v3);
		grafo.agregarVertice(v4);
		
		grafo.agregarPesosDeVertices(v1, 2.0);
		grafo.agregarPesosDeVertices(v2, 3.0);
		grafo.agregarPesosDeVertices(v3, 5.0);
		grafo.agregarPesosDeVertices(v4, 6.0);

		grafo.agregarArco(v1, v2);
		grafo.agregarArco(v1, v3);
		grafo.agregarArco(v1, v4);
		grafo.agregarArco(v2, v3);
		grafo.agregarArco(v2, v4);
		grafo.agregarArco(v3, v4);
		CliqueDePesoMaximo maxClique = new CliqueDePesoMaximo (grafo);
		Grafo maxCliqueObtenida = maxClique.obtenerCliquePesoMax();
		assertEquals(4, maxCliqueObtenida.consultarTamanio());
	}
	
	@Test
	public void obtenerCliquePesoMaxCasoFeliz2Test() {
		ArrayList<Integer> vertices = new ArrayList<>();
		Integer v1 = 2;
		Integer v2 = 3;
		Integer v3 = 8;
		Integer v4 = 6;
		Integer v5 = 1;
		Integer v6 = 7;
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		vertices.add(v5);
		vertices.add(v6);
		Grafo grafo = new Grafo();
		
		grafo.agregarVertice(v1);
		grafo.agregarVertice(v2);
		grafo.agregarVertice(v3);
		grafo.agregarVertice(v4);
		grafo.agregarVertice(v5);
		grafo.agregarVertice(v6);
		
		grafo.agregarPesosDeVertices(v1, 2.0);
		grafo.agregarPesosDeVertices(v2, 3.0);
		grafo.agregarPesosDeVertices(v3, 8.0);
		grafo.agregarPesosDeVertices(v4, 6.0);
		grafo.agregarPesosDeVertices(v5, 1.0);
		grafo.agregarPesosDeVertices(v6, 7.0);

		grafo.agregarArco(v1, v4);
		grafo.agregarArco(v1, v5);
		grafo.agregarArco(v2, v5);
		grafo.agregarArco(v2, v6);
		grafo.agregarArco(v3, v6);
		grafo.agregarArco(v4, v6);
		grafo.agregarArco(v4, v3);
		CliqueDePesoMaximo maxClique = new CliqueDePesoMaximo (grafo);
		Grafo maxCliqueObtenida = maxClique.obtenerCliquePesoMax();
		assertEquals(3, maxCliqueObtenida.consultarTamanio());
	}
	
	@Test
	public void quitarVerticeMenorPesoTest() {
		ArrayList<Integer> vertices = new ArrayList<>();
		Integer v1 = 2;
		Integer v2 = 3;
		Integer v3 = 5;
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		Grafo grafo = new Grafo();
		
		grafo.agregarVertice(v1);
		grafo.agregarVertice(v2);
		grafo.agregarVertice(v3);
		
		grafo.agregarPesosDeVertices(v1, 2.0);
		grafo.agregarPesosDeVertices(v2, 3.0);
		grafo.agregarPesosDeVertices(v3, 5.0);
		
		grafo.agregarArco(v1, v2);
		grafo.agregarArco(v2, v3);
		grafo.agregarArco(v3, v1);
		
		CliqueDePesoMaximo maxClique = new CliqueDePesoMaximo (grafo);
		assertEquals(3, grafo.consultarTamanio());
		maxClique.quitarVerticeMenorPeso();
		assertEquals(2, grafo.consultarTamanio());
		assertEquals(3, grafo.obtenerPesoDeVertice(grafo.consultarVertices().get(0)).intValue());
		assertEquals(5, grafo.obtenerPesoDeVertice(grafo.consultarVertices().get(1)).intValue());
	}
	
	@Test
	public void esCliqueTest() {;
		ArrayList<Integer> vertices = new ArrayList<>();
		Integer v1 = 2;
		Integer v2 = 3;
		Integer v3 = 5;
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		Grafo grafo = new Grafo();
		
		grafo.agregarVertice(v1);
		grafo.agregarVertice(v2);
		grafo.agregarVertice(v3);
		
		grafo.agregarPesosDeVertices(v1, 2.0);
		grafo.agregarPesosDeVertices(v2, 3.0);
		grafo.agregarPesosDeVertices(v3, 5.0);

		grafo.agregarArco(v1, v2);
		grafo.agregarArco(v2, v3);
		grafo.agregarArco(v3, v1);
		
		CliqueDePesoMaximo maxClique = new CliqueDePesoMaximo(grafo);
		assertTrue(maxClique.esClique());
	}
	
	@Test
	public void noEsCliqueTest() {;
		ArrayList<Integer> vertices = new ArrayList<>();
		Integer v1 = 2;
		Integer v2 = 3;
		Integer v3 = 5;
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		Grafo grafo = new Grafo();
		
		grafo.agregarVertice(v1);
		grafo.agregarVertice(v2);
		grafo.agregarVertice(v3);
		
		grafo.agregarPesosDeVertices(v1, 2.0);
		grafo.agregarPesosDeVertices(v2, 3.0);
		grafo.agregarPesosDeVertices(v3, 5.0);
		
		grafo.agregarArco(v1, v2);
		grafo.agregarArco(v2, v3);
		
		CliqueDePesoMaximo maxClique = new CliqueDePesoMaximo (grafo);
		assertFalse(maxClique.esClique());
	}
	
    @Test
    public void siExisteArcoTest() {
        Grafo grafo = new Grafo();

        assertFalse(grafo.existeArco(1, 2)); 
    }
	
}
