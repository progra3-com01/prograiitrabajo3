package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import model.Grafo;

public class GrafoTest {
    private Grafo grafo;

    @Before
    public void inicializar() {
        grafo = new Grafo();
        grafo.agregarVertice(1);
        grafo.agregarVertice(2);
        grafo.agregarVertice(3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void crearGrafoListaConNuloTest() {
        grafo = new Grafo();
        grafo.agregarVertice(1);
        grafo.agregarVertice(null); 
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void agregarVerticeNullTest() {
        grafo.agregarVertice(null);
    }

    @Test
    public void agregarAristaTest() {
        inicializar();
        grafo.agregarArco(1, 2);

        HashSet<Integer> vecinosEsperadosPrimerVertice = new HashSet<>();
        vecinosEsperadosPrimerVertice.add(2);
        HashSet<Integer> vecinosEsperadosSegundoVertice = new HashSet<>();
        vecinosEsperadosSegundoVertice.add(1);

        assertEquals(vecinosEsperadosPrimerVertice, grafo.obtenerVecinosDe(1));
        assertEquals(vecinosEsperadosSegundoVertice, grafo.obtenerVecinosDe(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void agregarAristaExistenteTest() {
        inicializar();
        grafo.agregarArco(1, 2);
        grafo.agregarArco(1, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void agregarAristaLoop() {
        inicializar();
        grafo.agregarArco(1, 1);
    }

    @Test
    public void completarGrafoUnicoVerticeTest() {
        grafo = new Grafo();
        grafo.agregarVertice(1);
        grafo.completar();
        assertEquals(0, grafo.consultarVecinos().get(0).size());
    }

    @Test
    public void completarGrafoDosVertices() {
        grafo = new Grafo();
        grafo.agregarVertice(1);
        grafo.agregarVertice(2);
        grafo.completar();

        assertEquals(2, grafo.consultarVecinos().size());
    }

    @Test
    public void completarGrafoNVertices() {
        inicializar();
        grafo.completar();
        int cantVerticesGrafo = 0;
        for (HashSet<Integer> conjunto : grafo.consultarVecinos())
            for (Integer vertice : conjunto)
                cantVerticesGrafo++;

        // En un grafo con n vértices, se generan n*(n-1)/2 aristas
        int cantVertices = grafo.consultarTamanio();
        int cantAristasEsperadas = cantVertices * (cantVertices - 1) / 2;
        assertEquals(cantAristasEsperadas, cantVerticesGrafo / 2);
    }

    @Test
    public void consultarVerticesTest() {
        inicializar();

        ArrayList<Integer> vertices = grafo.consultarVertices();

        ArrayList<Integer> verticesEsperados = new ArrayList<>();
        verticesEsperados.add(1);
        verticesEsperados.add(2);
        verticesEsperados.add(3);

        assertEquals(verticesEsperados, vertices);
    }

    @Test
    public void consultarExisteAristaTest() {
        inicializar();
        grafo.agregarArco(1, 2);

        assertTrue(grafo.existeArco(1, 2));
        assertTrue(grafo.existeArco(2, 1));
    }

    @Test
    public void consultarVecinosTest() {
        inicializar();
        grafo.completar();

        HashSet<Integer> vecinos = grafo.obtenerVecinosDe(1);
        HashSet<Integer> esperados = new HashSet<>();
        esperados.add(2);
        esperados.add(3);

        assertEquals(esperados, vecinos);
    }
}