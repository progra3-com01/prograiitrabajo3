# Trabajo Práctico 3: Cliques voraces (Programación III)

**Docentes:** Patricia Bagnes; Ignacio Sotelo (Com-01).  

## Descripción

El trabajo práctico consiste en implementar un algoritmo goloso para el problema de la clique de peso máximo.

Dado un grafo, una clique es un conjunto de vértices tal que todos los vértices del conjunto son vecinos entre sí. Suponemos, además, que tenemos un peso asociado con cada vértice, y el peso de una clique es igual a la suma de los pesos de sus vértices. El problema consiste en hallar una clique de peso máximo.

![Figura](figura1.png)

Se implementa una aplicación que, a partir de los datos del grafo, utilice un algoritmo goloso para encontrar una clique con el mayor peso posible. La aplicación informa la clique obtenida. Para el ingreso del grafo con sus pesos, se pueden implementar cualquiera de estas dos opciones:

1. Dar al usuario la opción de ir cargando el grafo manualmente, con botones para agregar vértices y para agregar arcos. Cuando se agrega un vértice, se debe pedir al usuario su peso. Cuando se agrega un arco, se debe pedir al usuario los dos extremos del arco (que deben ser vértices existentes).

2. Leer el grafo desde un archivo, con el formato que el grupo determine. Puede ser un archivo de texto plano o un archivo JSON, a elección del grupo.
